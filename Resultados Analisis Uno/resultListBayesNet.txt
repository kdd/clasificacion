=== Run information ===

Scheme:weka.classifiers.bayes.BayesNet -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5
Relation:     data_study-weka.filters.unsupervised.attribute.Remove-R4-weka.filters.unsupervised.attribute.ReplaceMissingValues-weka.filters.unsupervised.attribute.Discretize-B10-M-1.0-R1-weka.filters.unsupervised.attribute.Remove-R9-10-weka.filters.unsupervised.attribute.Discretize-B10-M-1.0-R9
Instances:    12426
Attributes:   11
              age
              workclass
              education
              marital-status
              occupation
              relationship
              race
              sex
              hours-per-week
              native-country
              class-attribute
Test mode:split 66.0% train, remainder test

=== Classifier model (full training set) ===

Bayes Network Classifier
not using ADTree
#attributes=11 #classindex=10
Network structure (nodes followed by parents)
age(10): class-attribute 
workclass(8): class-attribute 
education(16): class-attribute 
marital-status(7): class-attribute 
occupation(14): class-attribute 
relationship(6): class-attribute 
race(5): class-attribute 
sex(2): class-attribute 
hours-per-week(10): class-attribute 
native-country(41): class-attribute 
class-attribute(2): 
LogScore Bayes: -165882.37016879395
LogScore BDeu: -166687.11639535637
LogScore MDL: -166712.6249297517
LogScore ENTROPY: -165680.30860654844
LogScore AIC: -165899.30860654844


Time taken to build model: 0.19 seconds

=== Evaluation on test split ===
=== Summary ===

Correctly Classified Instances        3385               80.1183 %
Incorrectly Classified Instances       840               19.8817 %
Kappa statistic                          0.516 
Mean absolute error                      0.2186
Root mean squared error                  0.3823
Relative absolute error                 60.2727 %
Root relative squared error             90.1428 %
Total Number of Instances             4225     

=== Detailed Accuracy By Class ===

               TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
                 0.807     0.218      0.924     0.807     0.861      0.878    false
                 0.782     0.193      0.555     0.782     0.649      0.878    true
Weighted Avg.    0.801     0.212      0.837     0.801     0.811      0.878

=== Confusion Matrix ===

    a    b   <-- classified as
 2608  624 |    a = false
  216  777 |    b = true

