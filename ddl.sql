--ddl para las tablas de datos temporales
DROP SEQUENCE censo_temp_key_seq CASCADE;
DROP TABLE censo_temp CASCADE;


CREATE SEQUENCE censo_temp_key_seq
	INCREMENT 1
	MINVALUE 1
	START 1;

CREATE TABLE censo_temp
(
   censo_temp_key INTEGER DEFAULT NEXTVAL('censo_temp_key_seq'),
	age INTEGER,
	workclass VARCHAR(30),
	education VARCHAR(30),
	education_num INTEGER,
	marital_status VARCHAR(30),
	occupation VARCHAR (30),
	relationship VARCHAR(30),
	race VARCHAR(20),
	sex VARCHAR (10),
	capital_gain INTEGER, 
	capital_loss INTEGER,
	hours_per_week INTEGER,
	native_country VARCHAR(40),
	class_attribute BOOLEAN
	
);

ALTER TABLE censo_temp ADD CONSTRAINT censo_pk PRIMARY KEY (censo_temp_key);
